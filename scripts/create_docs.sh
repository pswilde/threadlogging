#!/bin/bash

rm -r docs/*
nim doc --project src/threadlogging.nim
mv src/htmldocs/* docs/

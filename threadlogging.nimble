# Package

version       = "0.1.5"
author        = "Paul Wilde"
description   = "A thread safe logging library using Nim's own logging module"
license       = "AGPL-3.0-or-later"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6.0"

task testlogs, "Runs the tests":
  exec "nim c -r tests/test.nim"

task testloggers, "Runs tests with different loggers":
  exec "nim c -r tests/testloggers.nim"

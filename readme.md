# Nim ThreadLogging
A thread safe logging library using Nim's own logging module.  
Creates a channel and listens for incoming messages which are then written to 
the console.  

[API Reference](https://pswilde.codeberg.page/threadlogging_docs/threadlogging.html)  

## What's this for?
Even though you can use Nim's own logging module for logging in threads, you need
to either pass a logging var into the thread or start a new Logger for each thread.  
This threadlogging module gets around this requirement by starting up a logger in its own 
thread and opens a channel which any other thread can communicate with. This all 
happens within the threadlogging module meaning all you need to do is call `initLogThread()` once, then `import threadlogging` in any file requiring it 
where you'll then be able to call the normal `info`, `warn`, `debug`, `error`, `fatal` and `notice` procedures to write logs

## How to use
Your application needs to initialize the logger once, this opens the channel 
and starts listening.  

Create loggers as per [Nim's logging Reference](https://nim-lang.org/docs/logging.html)   

```nim
import threadlogging

if isMainModule:
  # Create a logger to output debug into to the console
  var consoleLogger = newConsoleLogger(lvlDebug, "[$time] - $levelname: ")
  # And a file logger to output errors and above to file
  var fileLogger = newFileLogger("errors.log", levelThreshold = lvlError, fmtStr = "[$time] - $levelname: ")
  var loggers = @[consoleLogger, fileLogger]
  # Initialize the log thread
  initLogThread(loggers)
  # And you're good to log!
  notice "Here we go, logging is running!"
  # Go Off and do loads of other things
  doOtherStuff() # in other procedures, import threadlogging and use info, warn, debug, etc. logging procs
  # Run `finishLogging()` so all logs get written
  finishLogging()
```

Then, you can use the standard names of Nim's own logging module to write logs
``` nim
import threadlogging

proc someOtherThread() =
  sendLog lvlDebug, "You can use the `sendLog` procedure with the log level type"
  debug "Or call the log level type procedure directly"
```
## Defaults to use a consoleLogger
Running `initLogThread()` with no params initializes a standard `ConsoleLogger`

## Fatal logs close the channel and thread
When a Fatal log is received, the log is written and the thread then closes and also closes the channel. If you don't want this, simply initLogThread with `initLogThread(loggers, closeOnFatal = false)`

## Waiting for logging to finish
If all other threads end, then it's likely not all logs will be written before 
the application closes.  
To prevent this, call the `finishLogging()` procedure (which is really just a 
wrapper for `joinThread logThread` which causes the main thread to block until a
Fatal log is written

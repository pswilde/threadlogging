
import logging
import threadlogging as tl

var consoleLogger = newConsoleLogger(lvlDebug, "{$time}:$levelname ")
var fileLogger = newFileLogger("threadlogging.log", levelThreshold = lvlError, fmtStr = "[$time] - $levelname: ")
var loggers: seq[Logger] = @[consoleLogger, fileLogger]
initLogThread(loggers)
tl.notice "Starting Notice"
tl.info "Info Log"
tl.debug "Debug Log"
tl.warn "Warning Log"
tl.error "Error Log"
tl.fatal "Fatal Log"
finishLogging()

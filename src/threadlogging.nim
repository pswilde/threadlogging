## # Threadlogging
## A thread safe logging library using Nim's own logging module.
##   \
## Creates a channel and listens for incoming messages which are then written to 
## the console.
##   \
## ## How to use
## Your application needs to initialize the logger once, this opens the channel 
## and starts listening
## ```nim
## import threadlogging
##
## if isMainModule:
##   # Create a logger to output debug into to the console
##   var consoleLogger = newConsoleLogger(lvlDebug, "[$time] - $levelname: ")
##   # And a file loffer to output errors and above to file
##   var fileLogger = newFileLogger("errors.log", levelThreshold = lvlError, fmtStr = "[$time] - $levelname: ")
##   var loggers = @[consoleLogger, fileLogger]
##   initLogThread(loggers)
##   notice "Here we go, logging is running!"
##   # Go Off and do loads of other things
##   doOtherStuff() # in other procedures, import threadlogging and use info, warn, debug, etc. logging procs
##   # Run `finishLogging()` so all logs get written
##   finishLogging()
## ```
##
## Running `initLogThread()` with no params initializes a standard `ConsoleLogger`

import strutils
import logging

type
  LogMessage = object
    level: Level
    message: string
  LogVars = (seq[Logger], bool)
  LogLevel* = Level
    ## Exports Level so a level can be easily sent for initialisation
    ## lvlDebug, lvlInfo, lvlNotice, lvlWarn, lvlError, lvlFatal, lvlNone, lvlAll
var logChannel: Channel[LogMessage]
var logThread: Thread[LogVars]

proc fileTag(filename: string): string =
  var fileStr = filename
  if fileStr.contains(".nim"):
    fileStr = fileStr[0..^5]
  let m = "[$#] " % [fileStr]
  return m

proc toStr(args: varargs[string]): string =
  return args.join("")

template sendLog*(logLevel: Level, args: varargs[string,`$`]) =
  ## Passes the log level and log strings to the logChannel
  let msg = LogMessage(
    level: logLevel,
    message: args.toStr
  )
  logChannel.send msg

template info*(args: varargs[string, `$`]) =
  ## Writes an Info Log
  let file_tag = fileTag(instantiationInfo().filename)
  sendLog(lvlInfo, file_tag, args.toStr)

template notice*(args: varargs[string, `$`]) =
  ## Writes a Notice Log
  let file_tag = fileTag(instantiationInfo().filename)
  sendLog(lvlNotice, file_tag, args.toStr)

template debug*(args: varargs[string, `$`]) =
  ## Writes a Debug Log
  let file_tag = fileTag(instantiationInfo().filename)
  sendLog(lvlDebug, file_tag, args.toStr)

template warn*(args: varargs[string, `$`]) =
  ## Writes a Warn Log
  let file_tag = fileTag(instantiationInfo().filename)
  sendLog(lvlWarn, file_tag, args.toStr)

template error*(args: varargs[string, `$`]) =
  ## Writes a Error Log
  let file_tag = fileTag(instantiationInfo().filename)
  sendLog(lvlError, file_tag, args.toStr)

template fatal*(args: varargs[string, `$`]) =
  ## Writes a Fatal Log
  let file_tag = fileTag(instantiationInfo().filename)
  sendLog(lvlFatal, file_tag, args.toStr)


proc initLogListener(closeOnFatal: bool = true) =
  while true:
    let msg = recv logChannel
    logging.log(msg.level, msg.message)
    if closeOnFatal and msg.level == lvlFatal:
      logging.log(lvlNotice, "Shutting down Log Handler due to Fatal error")
      break

proc initLogHandler(logvars: LogVars) {.thread.} =
  let 
    loggers = logvars[0]
    closeOnFatal = logvars[1]
  for logger in loggers:
    addHandler(logger)
  initLogListener(closeOnFatal)

#proc initialized*(): bool =
#  ## Checks to see if we have some handlers, so the logging thread is active
#  return getHandlers().len < 1

proc finishLogging*() =
  ## Joins the log thread which should block the program exiting until all logs are written 
  ## For example, if all other threads finish processing, they may exit before all
  ## logs are written, calling this procedure should prevent this.
  joinThreads logThread
  logChannel.close()
  
proc initLogThread*(loggers: seq[Logger], closeOnFatal: bool = true) =
  ## Starts the logging thread by opening a communication channel
  ## and creating a thread.
  ##  \
  ## Requires a sequence of Loggers to be sent \
  ## i.e. newConsoleLogger, newFileLogger, newRollingFileLogger
  logChannel.open()
  #initLoggersHandler (loggers, closeOnFatal)
  createThread logThread, initLogHandler, (loggers, closeOnFatal)
  notice "Logger Started"

proc initLogThread*(fmtStr: string = "[$time] - $levelname: ",
                    levelThreshold: Level = lvlDebug,
                    closeOnFatal: bool = true) =
  ## Starts a simple Console Logger with default settings
  logChannel.open()
  var loggers: seq[Logger] = @[Logger(newConsoleLogger(fmtStr = fmtStr,
                                   levelThreshold = levelThreshold))]
  initLogThread(loggers,closeOnFatal)
  debug "Started with fmtStr: ", fmtStr
  debug "           logLevel: ", $levelThreshold
  debug "       closeOnFatal: ", $closeOnFatal



